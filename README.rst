===============================================
TU/e Visualization Assignment 2
===============================================

**By Chuan-Bin Huang (1342037) and Huilin Zhu (1378627)**

* View online: http://cbbh.42web.io/projects/suicide-rate/index.html
* Project page: https://gitlab.com/cbhuang/tue-vis-hw2


Development View
=======================

.. code-block:: bash

  git clone https://gitlab.com/cbhuang/tue-vis-hw2
  cd tue-vis-hw2
  python3 -m http.server
  # And then open http://localhost:8000 on your browser


Project Structure
=================================

* ``script/``: javascript for data preprocession and rendering
* ``data/``: dataset to be loaded directly (but not ``data/raw/``)
* ``template/``: html files of each tab
* ``static/``: css and images
* ``img/``: screenshots


Screenshots
========================


Interactive Data Inspector
----------------------------------

.. image:: img/tabulator.png
   :align: center
   :width: 600


Interactive Choropleth
-----------------------------

.. image:: img/choropleth_detail2.png
   :align: center
   :width: 600


Stratified Bar Chart
-----------------------------

.. image:: img/stackBar.png
   :align: center
   :width: 600


Time Series View
------------------------

.. image:: img/ts-age.png
   :align: center
   :width: 600


Scatter Plot View and Slidebar
------------------------------------

.. image:: img/timebar.png
   :align: center
   :width: 600

//
//  Make up NTILE() functional deficiency of alaSQL
//
//  NOTE: cannot do aggregation simultaneously!

// generate quantile for a variable (Overall)
function ntileGroupByNone(data, key, qName, desc=false, n=5) {
  // data: standard json array
  // key: the column to be quantized
  // qName: name of the quantile variable
  // desc: in descending order
  // n: quantile number (default=5)

  let len = data.length;  // array length
  let i;
  let j;

  // step 1. generate quantile breakpoints
  let arrQt = [];
  i = 0;
  while (i < n) {
    arrQt.push(Math.floor(i / n * len));
    i++;
  }
  arrQt.push(len);
  //console.log(arrQt);

  // step 2. sort array (n.b. no aggregation for now)
  if (desc === true) {
    data.sort( (a,b) => d3.descending(a[key], b[key]) );
  } else {
    data.sort( (a,b) => d3.ascending(a[key], b[key]) );
  }
  //console.log(data);

  // step 3. assign quantile
  let ans = [];
  let el;  // element
  i=0;
  while (i < len) {
    el = data[i];
    j = 0;
    while (j < n) {  // should use binary search when n is large
      if (i < arrQt[j+1]) {
        //console.log(`i=${i}, breakpoint index=${j}`);
        break;
      }
      j++;
    }
    el[qName] = j+1;  // give 1-based quantile
    ans.push(el);  // save
    i++;
  }

  return ans;
}


// generate quantile, group by one variable
function ntileGroupBy1(data, key, groupvar, qName, desc=false, n=5) {
  // data: standard json array
  // key: the column to be quantized
  // groupvar: group by which variable
  // qName: name of the quantile variable
  // desc: in descending order
  // n: quantile number (default=5)

  // step 1. group by (nest)
  let data_nested = d3.nest()
    .key( (d) => {return d[groupvar]} )
    .object(data);

  // assign quantile within each "nest"
  let ans = [];
  let el;  // element
  let data_gp;  // data from 1 group
  let len_gp;  // length of data_gp

  for (let k1 in data_nested) {  // asynchronous execution among groups

    data_gp = data_nested[k1];

    // step 2. generate quantile breakpoints
    let arrQt = [];
    let i = 0;
    while (i < n) {
      arrQt.push(Math.floor(i / n * data_gp.length));
      i++;
    }
    arrQt.push(data_gp.length);
    //console.log(arrQt);

    // step 3. sort array
    if (desc === true) {
      data_gp.sort( (a,b) => d3.descending(a[key], b[key]) );
    } else {
      data_gp.sort( (a,b) => d3.ascending(a[key], b[key]) );
    }
    //console.log(data_nested);

    // step 4. assign quantile (synchronous within a group)
    i = 0;
    while (i < data_gp.length) {
      el = data_gp[i];
      let j = 0;
      while (j < n) {  // test through breakpoints until i < breakpoint
        if (i < arrQt[j+1]) {
          //console.log(`i=${i}, breakpoint index=${j}`);
          break;
        }
        j++;
      }
      el[qName] = j+1;  // give 1-based quantile
      // (need not write grouping key back!)
      ans.push(el);  // save
      i++;
    }
  }

  return ans;
}

// generate quantile, group by one variable
function ntileGroupBy2(data, key, arr_groupvar, qName, desc=false, n=5) {
  // data: standard json array
  // key: the column to be quantized
  // arr_groupvar: group by which variable [var1, var2]
  // qName: name of the quantile variable
  // desc: in descending order
  // n: quantile number (default=5)

  // step 1. group by (nest)
  let data_nested = d3.nest()
    .key( (d) => {return d[arr_groupvar[0]]})
    .key( (d) => {return d[arr_groupvar[1]]})
    .object(data);

  // assign quantile within each "nest"
  let ans = [];
  let el;  // element
  let data_gp;  // data from 1 group
  let len_gp;  // length of data_gp

  for (let k1 in data_nested) {  // asynchronous execution among groups
    for (let k2 in data_nested[k1]) {

      data_gp = data_nested[k1][k2];

      // step 2. generate quantile breakpoints
      let arrQt = [];
      let i = 0;
      while (i < n) {
        arrQt.push(Math.floor(i / n * data_gp.length));
        i++;
      }
      arrQt.push(data_gp.length);
      //console.log(arrQt);

      // step 3. sort array
      if (desc === true) {
        data_gp.sort( (a,b) => d3.descending(a[key], b[key]) );
      } else {
        data_gp.sort( (a,b) => d3.ascending(a[key], b[key]) );
      }
      //console.log(data_nested);

      // step 4. assign quantile (synchronous within a group)
      i = 0;
      while (i < data_gp.length) {
        el = data_gp[i];
        let j = 0;
        while (j < n) {  // test through breakpoints until i < breakpoint
          if (i < arrQt[j+1]) {
            //console.log(`i=${i}, breakpoint index=${j}`);
            break;
          }
          j++;
        }
        el[qName] = j+1;  // give 1-based quantile
        // (need not write grouping key back!)
        ans.push(el);  // save
        i++;
      }
    }
  }

  return ans;
}

// generate quantile, group by one variable
function ntileGroupBy3(data, key, arr_groupvar, qName, desc=false, n=5) {
  // data: standard json array
  // key: the column to be quantized
  // arr_groupvar: group by which variable [var1, var2, var3]
  // qName: name of the quantile variable
  // desc: in descending order
  // n: quantile number (default=5)

  // step 1. group by (nest)
  let data_nested = d3.nest()
    .key( (d) => {return d[arr_groupvar[0]]})
    .key( (d) => {return d[arr_groupvar[1]]})
    .key( (d) => {return d[arr_groupvar[2]]})
    .object(data);

  // assign quantile within each "nest"
  let ans = [];
  let el;  // element
  let data_gp;  // data from 1 group
  let len_gp;  // length of data_gp

  for (let k1 in data_nested) {  // asynchronous execution among groups
    for (let k2 in data_nested[k1]) {
      for (let k3 in data_nested[k1][k2]) {

        data_gp = data_nested[k1][k2][k3];

        // step 2. generate quantile breakpoints
        let arrQt = [];
        let i = 0;
        while (i < n) {
          arrQt.push(Math.floor(i / n * data_gp.length));
          i++;
        }
        arrQt.push(data_gp.length);
        //console.log(arrQt);

        // step 3. sort array
        if (desc === true) {
          data_gp.sort( (a,b) => d3.descending(a[key], b[key]) );
        } else {
          data_gp.sort( (a,b) => d3.ascending(a[key], b[key]) );
        }
        //console.log(data_nested);

        // step 4. assign quantile (synchronous within a group)
        i = 0;
        while (i < data_gp.length) {
          el = data_gp[i];
          let j = 0;
          while (j < n) {  // test through breakpoints until i < breakpoint
            if (i < arrQt[j+1]) {
              //console.log(`i=${i}, breakpoint index=${j}`);
              break;
            }
            j++;
          }
          el[qName] = j+1;  // give 1-based quantile
          // (need not write grouping key back!)
          ans.push(el);  // save
          i++;
        }
      }  // for k3
    }
  }

  return ans;
}

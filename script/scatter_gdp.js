//
//  Multivariate Analysis: GDP vs Suicide Rate Scatter Plot
//

function initGdpSrateScatterPlot() {
  $("#main").load("template/scatter_gdp.html", function() {
    updateGdpSrateScatterPlot();
  });
}

// markerSize by p
function markerSize(minSize=10, maxSize=50) {
  let data = window.avgPopulation;
  let minVal = d3.min(unpackArray(data, "population"));
  let maxVal = d3.max(unpackArray(data, "population"));
  let size;
  let i = 0;
  let ans = [];
  while (i < data.length) {
    // linear interpolation for marker size
    size = minSize + (maxSize - minSize) * (data[i].population - minVal) / (maxVal - minVal);
    ans.push(size);
    ++i;
  }
  return ans;
}

function updateGdpSrateScatterPlot() {

  let data = window.dataByCountry;
  // show country with population
  let text = [];
  let tmp_text;
  let i = 0;
  let popM = 0;
  while (i < data.length) {
    popM = window.avgPopulation[i].population / 1000000;
    tmp_text = `${data[i].country}, population=${popM.toFixed(1)}M`;
    text.push(tmp_text);
    ++i;
  }

  // data to plot
  let trace1 = {
    x: data.map( (d) => {return d.gdpPc.toPrecision(5)} ),
    y: data.map( (d) => {return d.srate.toFixed(2)} ),
    type: 'scatter',
    mode: 'markers',
    text: text, // country name
    marker: {
      size: markerSize(),
      //color: '#778899',
    }
  };
  let data_plot = [trace1];

  // layout
  let layout = {
    title: "Suicide Rate vs GDP per Capita",
    barmode: 'stack',
    hovermode: 'closest',
    //showlegend: false,
    width: 800,
    height: 600,
    // margin: {
    //   t: 100,
    // },
    font: {
      size: 24,
    },
    xaxis: {
      title: "GDP per Capita",
      tickfont: {
        size: 18,
      },
      titlefont: {
        size: 18,
      },
    },
    yaxis: {
      title: 'Suicide Rate per 100k Person per Year',
      titlefont: {
        size: 18,
      },
      tickfont: {
        size: 18,
      }
    },
    annotations: [
      {
        xref: 'paper',
        yref: 'paper',
        x: 0.55,
        y: 1.08,
        xanchor: 'center',
        yanchor: 'center',
        text: "Circle Size: Average Population 1990-2013",
        font:{
          size: 18,
        },
        showarrow: false
      }
    ]
  };

  Plotly.newPlot("fig-srate-gdp", data_plot, layout);
}

//
// Common utility functions
//

// capitalize first letter
function capFirst(str) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}

// capitalize first letter
function toNumberIfPossible(str) {
  let num = Number(str);
  if (isNaN(num)) {
    return str;
  } else {
    return num;
  }
}

// get distinct element for list
function getDistinctList(data, var1, reverse=false) {
  let col = data.map( (d) => {return d[var1]});
  if (reverse) {
    window[`ls_${var1}`] = [...new Set(col)].sort().reverse();
  } else {
    window[`ls_${var1}`] = [...new Set(col)].sort();
  }
}

// unpack colorpleth data (plotly fixture)
function unpackArray(arr, key) {
    return arr.map( (row) => { return row[key];} );
}

// add gdp quantile and sorted by country name
// function addGdpQuantileByCountry(vname, n=5) {
//   // vname: name of target data set
//
//   // get quantile
//   let tmparr = ntileGroupByNone(window[vname], "gdpPc", desc=false, n=n);
//   // sort by country
//   window[vname] = tmparr.sort(sortByCountry);
// }


// Summary function, no grouping variables
function groupByNone(data) {

  // aggregate
  let tmp_data = d3.nest()
    .rollup(function(v) { return {
      // gdpPc: v["gdpPc"],
      gdpPc: d3.mean(v, (d) => { return d.gdpPc; } ),
      sn: d3.sum(v, (d) => { return d.sn; } ),
      population: d3.sum(v, (d) => { return d.population; } ),
    };
    })
    .object(data);

  // reformat and save
  tmp_data["srate"] = tmp_data["sn"] / tmp_data["population"] * 100000;

  window["dataByNone"] = tmp_data
}


// group by 1 variable
function groupBy1(data, var1) {

  // name of global variable to save
  let vname = `dataBy${capFirst(var1)}`;

  // aggregate
  let tmp_data = d3.nest()
    .key(function(d) { return d[var1]; })
    .rollup(function(v) { return {
      gdpPc: d3.mean(v, (d) => { return d.gdpPc; } ),
      sn: d3.sum(v, (d) => { return d.sn; } ),
      population: d3.sum(v, (d) => { return d.population; } ),
    };
    })
    .object(data);

  // reformat and save
  window[vname] = [];
  let tmp_obj;
  for (const key of Object.keys(tmp_data)) {  // ES6
    tmp_obj = {};
    // from tmp_data
    tmp_obj[var1] = toNumberIfPossible(key);
    tmp_obj["sn"] = tmp_data[key]["sn"];
    tmp_obj["population"] = tmp_data[key]["population"];
    tmp_obj["gdpPc"] = tmp_data[key]["gdpPc"];
    // mean Suicidel rate (per 100k)
    tmp_obj["srate"] = tmp_obj["sn"] / tmp_obj["population"] * 100000;
    window[vname].push(tmp_obj);
  }

  // if var1 is country, calculate gdp grouping
  // if (var1 == "country") {
  //   addGdpQuantileByCountry(vname, n=5);
  // }
}


// group by 2 variables
function groupBy2(data, var1, var2) {

  // name of global variable to save
  let vname = `dataBy${capFirst(var1)}${capFirst(var2)}`;

  // aggregate
  let tmp_data = d3.nest()
    .key(function(d) { return d[var1]; })
    .key(function(d) { return d[var2]; })
    .rollup(function(v) { return {
      gdpPc: d3.mean(v, (d) => { return d.gdpPc; } ),
      sn: d3.sum(v, (d) => { return d.sn; } ),
      population: d3.sum(v, (d) => { return d.population; } ),
      };
    })
    .object(data);

  // reform nest object (yes, d3 is too SMART, and groupBy is too dumb)
  window[vname] = [];
  let tmp_obj;
  for (const key1 of Object.keys(tmp_data)) {
    for (const key2 of Object.keys(tmp_data[key1])) {
      tmp_obj = {};
      // from tmp_data
      tmp_obj[var1] = toNumberIfPossible(key1);
      tmp_obj[var2] = toNumberIfPossible(key2);
      tmp_obj["sn"] = tmp_data[key1][key2]["sn"];
      tmp_obj["population"] = tmp_data[key1][key2]["population"];
      tmp_obj["gdpPc"] = tmp_data[key1][key2]["gdpPc"];
      // mean Suicidel rate (per 100k)
      tmp_obj["srate"] = tmp_obj["sn"] / tmp_obj["population"] * 100000;
      window[vname].push(tmp_obj);
    }
  }

  // if var1 is country, calculate gdp grouping
  // if (var1 == "country") {
  //   addGdpQuantileByCountry(vname, n=5);
  // }
}

// group by 3 variables
function groupBy3(data, var1, var2, var3) {

  // name of global variable to save
  let vname = `dataBy${capFirst(var1)}${capFirst(var2)}${capFirst(var3)}`;

  // aggregate
  let tmp_data = d3.nest()
    .key(function(d) { return d[var1]; })
    .key(function(d) { return d[var2]; })
    .key(function(d) { return d[var3]; })
    .rollup(function(v) { return {
      gdpPc: d3.mean(v, (d) => { return d.gdpPc; } ),
      sn: d3.sum(v, (d) => { return d.sn; } ),
      population: d3.sum(v, (d) => { return d.population; } ),
      };
    })
    .object(data);

  // reform nest object (yes, d3 is too SMART, and groupBy is too dumb)
  window[vname] = [];
  let tmp_obj;
  for (const key1 of Object.keys(tmp_data)) {
    for (const key2 of Object.keys(tmp_data[key1])) {
      for (const key3 of Object.keys(tmp_data[key1][key2])) {
        tmp_obj = {};
        // from tmp_data
        tmp_obj[var1] = toNumberIfPossible(key1);
        tmp_obj[var2] = toNumberIfPossible(key2);
        tmp_obj[var3] = toNumberIfPossible(key3);
        tmp_obj["sn"] = tmp_data[key1][key2][key3]["sn"];
        tmp_obj["population"] = tmp_data[key1][key2][key3]["population"];
        tmp_obj["gdpPc"] = tmp_data[key1][key2][key3]["gdpPc"];
        // mean Suicidel rate (per 100k)
        tmp_obj["srate"] = tmp_obj["sn"] / tmp_obj["population"] * 100000;
        window[vname].push(tmp_obj);
      }
    }
  }

  // if var1 is country, calculate gdp grouping
  // if (var1 == "country") {
  //   addGdpQuantileByCountry(vname, n=5);
  // }
}

// create Dropdown button
function createDropdown(var1) {

  let elemId = `#${var1}Dropdown`;
  let lsName = `ls_${var1}`;
  let dropdown = $(elemId);
  // cleanup (avoid duplicate items)
  dropdown.empty();
  // Add a dummy value
  dropdown.append(`<option selected="true" disabled>select ${var1}</option>`);
  // Populate dropdown with distinct values of the variable
  $.each(window[lsName], function(k, v) {
     dropdown.append($('<option></option>').val(v).html(v));
  });
  // set initial value
  dropdown.prop('selectedIndex', 1);
}

//
//  basic grouping (histograms)
//

function initTimeSeries() {
  $("#main").load("template/time_series.html", function (){
    initTimeSeriesBySexYear();
  });
}

//
//  1. Sex-Year
//
function initTimeSeriesBySexYear() {

  // double grouping
  groupBy2(window.data, "sex", "year");
  let data = window.dataBySexYear;

  // stacked components
  let y1 = data.filter( (d) => {return d.sex == "F"});
  let trace1 = {
    x: y1.map( (d) => {return d.year} ),
    y: y1.map( (d) => {return d.srate} ),
    name: 'Female',
    type: 'scatter',
  };

  let y2 = data.filter( (d) => {return d.sex == "M"});
  let trace2 = {
    x: y2.map( (d) => {return d.year} ),
    y: y2.map( (d) => {return d.srate} ),
    name: 'Male',
    type: 'scatter',
    // marker: {
    //   color: "#350c96",
    //   opacity: 0.8
    // },
  };

  let data_plot = [trace1, trace2];

  // layout
  let layout = {
    title: "Suicide Rate Time Series (by Sex)",
    barmode: 'stack',
    //showlegend: false,
    width: 800,
    height: 600,
    // margin: {
    //   t: 100,
    // },
    font: {
      size: 24,
    },
    xaxis: {
      title: "Year",
      tickfont: {
        size: 18,
      },
      titlefont: {
        size: 18,
      },
    },
    yaxis: {
      title: 'Suicide Rate per 100k Person per Year',
      titlefont: {
        size: 18,
      },
      tickfont: {
        size: 18,
      }
    },
  };

  // Refresh figure (.plot() will mess up legend)
  Plotly.newPlot("fig-basic-grouping", data_plot, layout);
}

//
//  2. Age-Year
//
function initTimeSeriesByAgeYear() {

  // double grouping
  groupBy2(window.data, "age", "year");
  let data = window.dataByAgeYear;

  // stacked components
  let y1 = data.filter( (d) => {return d.age == "05-14 years"} );
  let trace1 = {
    x: y1.map( (d) => {return d.year} ),
    y: y1.map( (d) => {return d.srate}),
    name: '05-14 years',
    type: 'scatter',
  };

  let y2 = data.filter( (d) => {return d.age == "15-24 years"});
  let trace2 = {
    x: y2.map( (d) => {return d.year} ),
    y: y2.map( (d) => {return d.srate}),
    name: '15-24 years',
    type: 'scatter',
  };

  let y3 = data.filter( (d) => {return d.age == "25-34 years"});
  let trace3 = {
    x: y3.map( (d) => {return d.year} ),
    y: y3.map( (d) => {return d.srate}),
    name: '25-34 years',
    type: 'scatter',
  };

  let y4 = data.filter( (d) => {return d.age == "35-54 years"});
  let trace4 = {
    x: y4.map( (d) => {return d.year} ),
    y: y4.map( (d) => {return d.srate}),
    name: '35-54 years',
    type: 'scatter',
  };

  let y5 = data.filter( (d) => {return d.age == "55-74 years"});
  let trace5 = {
    x: y5.map( (d) => {return d.year} ),
    y: y5.map( (d) => {return d.srate}),
    name: '55-74 years',
    type: 'scatter',
  };

  let y6 = data.filter( (d) => {return d.age == "75+ years"});
  let trace6 = {
    x: y6.map( (d) => {return d.year} ),
    y: y6.map( (d) => {return d.srate}),
    name: '75+ years',
    type: 'scatter',
  };

  let data_plot = [trace1, trace2, trace3, trace4, trace5, trace6];

  // layout
  let layout = {
    title: "Suicide Rate Time Series (by Age)",
    barmode: 'stack',
    //showlegend: false,
    width: 800,
    height: 600,
    // margin: {
    //   t: 100,
    // },
    font: {
      size: 24,
    },
    xaxis: {
      title: "Year",
      tickfont: {
        size: 18,
      },
      titlefont: {
        size: 18,
      },
    },
    yaxis: {
      title: 'Suicide Rate per 100k Person per Year',
      titlefont: {
        size: 18,
      },
      tickfont: {
        size: 18,
      }
    },
  };

  // Refresh figure (.plot() will mess up legend)
  Plotly.newPlot("fig-basic-grouping", data_plot, layout);
}

//
//  3. GdpGroup5 - Year
//

function initTimeSeriesByGdpPcGroup5Year() {

  // double grouping
  groupBy2(window.data, "gdpPcGroup5ByYear", "year");
  let data = window.dataByGdpPcGroup5ByYearYear;

  // stacked components
  let y1 = data.filter( (d) => {return d.gdpPcGroup5ByYear == 1} );
  let trace1 = {
    x: y1.map( (d) => {return d.year} ),
    y: y1.map( (d) => {return d.srate}),
    name: 'Group 1 (Low)',
    type: 'scatter',
  };

  let y2 = data.filter( (d) => {return d.gdpPcGroup5ByYear == 2} );
  let trace2 = {
    x: y2.map( (d) => {return d.year} ),
    y: y2.map( (d) => {return d.srate}),
    name: 'Group 2',
    type: 'scatter',
  };

  let y3 = data.filter( (d) => {return d.gdpPcGroup5ByYear == 3} );
  let trace3 = {
    x: y3.map( (d) => {return d.year} ),
    y: y3.map( (d) => {return d.srate}),
    name: 'Group 3',
    type: 'scatter',
  };

  let y4 = data.filter( (d) => {return d.gdpPcGroup5ByYear == 4} );
  let trace4 = {
    x: y4.map( (d) => {return d.year} ),
    y: y4.map( (d) => {return d.srate}),
    name: 'Group 4',
    type: 'scatter',
  };

  let y5 = data.filter( (d) => {return d.gdpPcGroup5ByYear == 5} );
  let trace5 = {
    x: y5.map( (d) => {return d.year} ),
    y: y5.map( (d) => {return d.srate}),
    name: 'Group 5 (High)',
    type: 'scatter',
  };

  let data_plot = [trace1, trace2, trace3, trace4, trace5];

  // layout
  let layout = {
    title: "Suicide Rate Time Series (by Age)",
    barmode: 'stack',
    //showlegend: false,
    width: 800,
    height: 600,
    // margin: {
    //   t: 100,
    // },
    font: {
      size: 24,
    },
    xaxis: {
      title: "Year",
      tickfont: {
        size: 18,
      },
      titlefont: {
        size: 18,
      },
    },
    yaxis: {
      title: 'Suicide Rate per 100k Person per Year',
      titlefont: {
        size: 18,
      },
      tickfont: {
        size: 18,
      }
    },
  };

  // Refresh figure (.plot() will mess up legend)
  Plotly.newPlot("fig-basic-grouping", data_plot, layout);
}

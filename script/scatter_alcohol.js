//
//  Multivariate Analysis: GDP vs Suicide Rate Scatter Plot
//

function initAlcoholPage() {

  $('#main').load("template/scatter_alcohol.html", function(){
    // set slider
    $('#yearSelect').slider({
      formatter: function(value) {
        return `Year: ${value}`;
      }
    });
    // initial plot
    updateAlcoholScatterPlot();
  });
}


function updateAlcoholScatterPlot() {

  let year = parseInt($('#yearSelect').val());

  let data = window.dataByCountryYear.filter( (d) => {
      if (d.year == year) {return true;} else {return false;}
    }).sort( (a,b) => d3.ascending(a.country, b.country) );

  //let data_gdp = data.map( (d) => {return d.gdpPc;} );
  //let data_srate = data.map( (d) => {return d.srate;} );
  let data_alcohol = window.dataAlcohol.filter( (d) => {
      if (d.year == year) {return true;} else {return false;}
    }).sort( (a,b) => d3.ascending(a.country, b.country) );

  // show gdp in text
  let text = [];
  let tmp_text;
  let i = 0;
  let v = 0;
  while (i < data.length) {
    v = data[i].gdpPc;
    tmp_text = `${data[i].country}, GDP per Capita=${v.toFixed(1)}`;
    text.push(tmp_text);
    ++i;
  }

  // data to plot
  let trace1 = {
    x: data_alcohol.map( (d) => {return d.liter} ),
    y: data.map( (d) => {return d.srate.toFixed(2)} ),
    type: 'scatter',
    mode: 'markers',
    text: text,
    marker: {
      size: markerSizeAlcoholPage(data),  // for this page
      //color: '#778899',
    }
  };
  let data_plot = [trace1];

  // layout
  let layout = {
    title: "Suicide Rate vs Alcohol Consumption",
    barmode: 'stack',
    hovermode: 'closest',
    //showlegend: false,
    width: 800,
    height: 600,
    // margin: {
    //   t: 100,
    // },
    font: {
      size: 24,
    },
    xaxis: {
      title: "Alcohol Consumption (Liter per Person per Year)",
      tickfont: {
        size: 18,
      },
      titlefont: {
        size: 18,
      },
    },
    yaxis: {
      title: 'Suicide Rate per 100k Person per Year',
      titlefont: {
        size: 18,
      },
      tickfont: {
        size: 18,
      }
    },
    annotations: [
      {
        xref: 'paper',
        yref: 'paper',
        x: 0.55,
        y: 1.08,
        xanchor: 'center',
        yanchor: 'center',
        text: `Circle Size: GDP per Capita, ${year}`,
        font:{
          size: 18,
        },
        showarrow: false
      }
    ]
  };

  Plotly.newPlot("fig-srate-alcohol", data_plot, layout);
}

// markerSize by p
function markerSizeAlcoholPage(data, minSize=10, maxSize=50) {
  // data: window.dataAlcohol, filtered by year, sorted by country

  let arr = unpackArray(data, "gdpPc");
  let minVal = d3.min(arr);
  let maxVal = d3.max(arr);
  let size;
  let i = 0;
  let ans = [];
  while (i < data.length) {
    // linear interpolation for marker size
    size = minSize + (maxSize - minSize) * (arr[i] - minVal) / (maxVal - minVal);
    ans.push(size);
    ++i;
  }
  return ans;
}

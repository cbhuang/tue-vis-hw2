//
// Figure 1: gengraphic distribution
// Interaction: year, gender, group

// create caption and element
function initWorldmap(){

  $("#main").load("template/worldmap_detail.html", function() {
    /* populate dropdown list  */
    createDropdown("year");
    createDropdown("age");
    createDropdown("sex");
    /* Initial plot */
    updateWorldmap();
  });
}

// plot interactively
function updateWorldmap(){

  // the user-selected value:
  let year = $("#yearDropdown").val();
  let age = $("#ageDropdown").val();
  let sex = $("#sexDropdown").val();
  //console.log(`Year=${year}, age=${age}, sex=${sex}`);

  // filter data
  let data = window.data.filter((d) => {
    if (d["year"] == year &&
        d["age"] == age &&
        d["sex"] == sex) {return true;}
    else {return false;};
  });
  //console.log(data);

  // data
  let data_plot = [{
      type: 'choropleth',
      locationmode: 'country names',
      locations: unpackArray(data, 'country'),
      z: unpackArray(data, 'srate'),
      //text: unpackArray(data, 'country'),
      //autocolorscale: true
      colorscale: 'Picnic',
  }];

  // layout
  let layout = {
      title: {
        text: `Suicide Rate - Detailed View`,
        size: 24,
        y: 0.93,  // avoid blocking interactive features
      },
      geo: {
          projection: {
              type: 'equirectangular'
          }
      },
      height: 500,
      width: 800,
      margin: {
        t: 70,
        b: 10,
        l: 10,
        r: 10
      },
      font: {
        size: 18,
      },
      annotations: [
        {
          xref: 'paper',
          yref: 'paper',
          x: 0.55,
          y: 1.025,
          xanchor: 'center',
          yanchor: 'center',
          text: `per 100k Person per Year<br>Year=${year}   Sex=${sex}   Age=${age}`,
          font:{
            size: 18,
          },
          showarrow: false
        }
      ]
  };

  // Refresh figure (.plot() will mess up legend)
  Plotly.newPlot("fig-worldmap", data_plot, layout, {showLink: false});
};

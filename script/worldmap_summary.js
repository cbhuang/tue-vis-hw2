//
// Figure 2: geographic distribution (aggregated)
// Interaction: year, gender, group
//

// create caption and element
function initWorldmapSummary(){
  initWorldmapByNone();
}

//
// 0. Overall Overall
//
function initWorldmapByNone(){
  $("#main").load("template/worldmap_summary.html", function() {
    $("#noneDropdown").show();
    $("#yearDropdown").hide();
    $("#ageDropdown").hide();
    $("#sexDropdown").hide();
    $("#generationDropdown").hide();
    // plot
    updateWorldmapByNone();
  });
}
function updateWorldmapByNone(){

  // no filter
  let data = window.dataByCountry;

  // data
  let data_plot = [{
      type: 'choropleth',
      locationmode: 'country names',
      locations: unpackArray(data, 'country'),
      z: unpackArray(data, 'srate'),
      //text: unpackArray(data, 'country'),
      colorscale: 'Picnic',
  }];

  // layout
  let layout = {
      title: {
        text: `<b>Suicide Rate (Overall)</b>`,
        size: 28,
        y: 0.93,  // avoid blocking interactive features
      },
      geo: {
          projection: {
              type: 'equirectangular'
          }
      },
      height: 500,
      width: 800,
      margin: {
        t: 70,
        b: 10,
        l: 10,
        r: 10
      },
      font: {
        size: 18,
      },
      annotations: [
        {
          xref: 'paper',
          yref: 'paper',
          x: 0.55,
          y: 1.025,
          xanchor: 'center',
          yanchor: 'center',
          text: `per 100k Person per Year<br>1990 - 2013`,
          font:{
            size: 18,
          },
          showarrow: false
        }
      ]
  };

  // Refresh figure (.plot() will mess up legend)
  Plotly.newPlot("fig-worldmap", data_plot, layout, {showLink: false});
};

//
// 1. By Year
//
function initWorldmapByYear(){
  $("#main").load("template/worldmap_agg.html", function() {
    createDropdown("year");
    $("#noneDropdown").hide();
    $("#yearDropdown").show();
    $("#ageDropdown").hide();
    $("#sexDropdown").hide();
    $("#generationDropdown").hide();
    // plot
    updateWorldmapByYear();
  });
}
function updateWorldmapByYear(){

  // the user-selected value:
  let year = $("#yearDropdown").val();
  // console.log(`Year=${year}`);

  // filter data
  let data = window.dataByCountryYear.filter((d) => {
      if (d["year"] == year) {return true;}
      else {return false;};
    });
  //console.log(data);

  // data
  let data_plot = [{
      type: 'choropleth',
      locationmode: 'country names',
      locations: unpackArray(data, 'country'),
      z: unpackArray(data, 'srate'),
      //text: unpackArray(data, 'country'),
      colorscale: 'Picnic',
  }];

  // layout
  let layout = {
      title: {
        text: `<b>Suicide Rate (by Year)</b>`,
        size: 28,
        y: 0.93,  // avoid blocking interactive features
      },
      geo: {
          projection: {
              type: 'equirectangular'
          }
      },
      height: 500,
      width: 800,
      margin: {
        t: 70,
        b: 10,
        l: 10,
        r: 10
      },
      font: {
        size: 18,
      },
      annotations: [
        {
          xref: 'paper',
          yref: 'paper',
          x: 0.55,
          y: 1.025,
          xanchor: 'center',
          yanchor: 'center',
          text: `per 100k Person per Year<br>Year=${year}`,
          font:{
            size: 18,
          },
          showarrow: false
        }
      ]
  };

  // Refresh figure (.plot() will mess up legend)
  Plotly.newPlot("fig-worldmap", data_plot, layout, {showLink: false});
};

//
// 2. By Age
//
function initWorldmapByAge(){
  $("#main").load("template/worldmap_agg.html", function() {
    createDropdown("age");
    $("#noneDropdown").hide();
    $("#yearDropdown").hide();
    $("#ageDropdown").show();
    $("#sexDropdown").hide();
    $("#generationDropdown").hide();
    // plot
    updateWorldmapByAge();
  });
}
function updateWorldmapByAge(){

  // the user-selected value:
  let age = $("#ageDropdown").val();
  // console.log(`Year=${year}`);

  // filter data
  let data = window.dataByCountryAge.filter((d) => {
      if (d["age"] == age) {return true;}
      else {return false;};
    });
  //console.log(data);

  // data
  let data_plot = [{
      type: 'choropleth',
      locationmode: 'country names',
      locations: unpackArray(data, 'country'),
      z: unpackArray(data, 'srate'),
      //text: unpackArray(data, 'country'),
      colorscale: 'Picnic',
  }];

  // layout
  let layout = {
      title: {
        text: `<b>Suicide Rate (by Age)</b>`,
        size: 28,
        y: 0.93,  // avoid blocking interactive features
      },
      geo: {
          projection: {
              type: 'equirectangular'
          }
      },
      height: 500,
      width: 800,
      margin: {
        t: 70,
        b: 10,
        l: 10,
        r: 10
      },
      font: {
        size: 18,
      },
      annotations: [
        {
          xref: 'paper',
          yref: 'paper',
          x: 0.55,
          y: 1.025,
          xanchor: 'center',
          yanchor: 'center',
          text: `per 100k Person per Year<br>Age=${age}`,
          font:{
            size: 18,
          },
          showarrow: false
        }
      ]
  };

  // Refresh figure (.plot() will mess up legend)
  Plotly.newPlot("fig-worldmap", data_plot, layout, {showLink: false});
};


//
// 3. By Sex
//
function initWorldmapBySex(){
  $("#main").load("template/worldmap_agg.html", function() {
    createDropdown("sex");
    $("#noneDropdown").hide();
    $("#yearDropdown").hide();
    $("#ageDropdown").hide();
    $("#sexDropdown").show();
    $("#generationDropdown").hide();
    // plot
    updateWorldmapBySex();
  });
}
function updateWorldmapBySex(){

  // the user-selected value:
  let sex = $("#sexDropdown").val();
  // console.log(`Year=${year}`);

  // filter data
  let data = window.dataByCountrySex.filter((d) => {
      if (d["sex"] == sex) {return true;}
      else {return false;};
    });
  //console.log(data);

  // data
  let data_plot = [{
      type: 'choropleth',
      locationmode: 'country names',
      locations: unpackArray(data, 'country'),
      z: unpackArray(data, 'srate'),
      //text: unpackArray(data, 'country'),
      colorscale: 'Picnic',
  }];

  // layout
  let layout = {
      title: {
        text: `<b>Suicide Rate (by Sex)</b>`,
        size: 28,
        y: 0.93,  // avoid blocking interactive features
      },
      geo: {
          projection: {
              type: 'equirectangular'
          }
      },
      height: 500,
      width: 800,
      margin: {
        t: 70,
        b: 10,
        l: 10,
        r: 10
      },
      font: {
        size: 18,
      },
      annotations: [
        {
          xref: 'paper',
          yref: 'paper',
          x: 0.55,
          y: 1.025,
          xanchor: 'center',
          yanchor: 'center',
          text: `per 100k Person per Year<br>Sex=${sex}`,
          font:{
            size: 18,
          },
          showarrow: false
        }
      ]
  };

  // Refresh figure (.plot() will mess up legend)
  Plotly.newPlot("fig-worldmap", data_plot, layout, {showLink: false});
};

//
// 4. By Generation
//
function initWorldmapByGeneration(){
  $("#main").load("template/worldmap_agg.html", function() {
    createDropdown("generation");
    $("#noneDropdown").hide();
    $("#yearDropdown").hide();
    $("#ageDropdown").hide();
    $("#sexDropdown").hide();
    $("#generationDropdown").show();
    // plot
    updateWorldmapByGeneration();
  });
}

function updateWorldmapByGeneration(){

  // the user-selected value:
  let generation = $("#generationDropdown").val();
  // console.log(`Year=${year}`);

  // filter data
  let data = window.dataByCountryGeneration.filter((d) => {
      if (d["generation"] == generation) {return true;}
      else {return false;};
    });
  //console.log(data);

  // data
  let data_plot = [{
      type: 'choropleth',
      locationmode: 'country names',
      locations: unpackArray(data, 'country'),
      z: unpackArray(data, 'srate'),
      //text: unpackArray(data, 'country'),
      colorscale: 'Picnic',
  }];

  // layout
  let layout = {
      title: {
        text: `<b>Suicide Rate (by Generation)</b>`,
        size: 28,
        y: 0.93,  // avoid blocking interactive features
      },
      geo: {
          projection: {
              type: 'equirectangular'
          }
      },
      height: 500,
      width: 800,
      margin: {
        t: 70,
        b: 10,
        l: 10,
        r: 10
      },
      font: {
        size: 18,
      },
      annotations: [
        {
          xref: 'paper',
          yref: 'paper',
          x: 0.55,
          y: 1.025,
          xanchor: 'center',
          yanchor: 'center',
          text: `per 100k Person per Year<br>Generation: ${generation}`,
          font:{
            size: 18,
          },
          showarrow: false
        }
      ]
  };

  // Refresh figure (.plot() will mess up legend)
  Plotly.newPlot("fig-worldmap", data_plot, layout, {showLink: false});
};

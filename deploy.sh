#!/bin/bash
#
#  Upload to cbbh.site
#

declare -a arr=(
  "favicon.ico"
  "index.html"
  "static"
  "script"
  "data"
  "template"
)

for i in "${arr[@]}"; do
    rsync -ave "ssh -i ~/.ssh/cbbhsite" --delete "$i" admin@cbbh.site:/var/www/html/
done
